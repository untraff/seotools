from django.apps import AppConfig


class StopwordsConfig(AppConfig):
    name = 'stopwords'
