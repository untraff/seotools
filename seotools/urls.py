from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
import registerManager.views
import descriptionGenerator.views
import titleGenerator.views
import stopwords.views
import adsgen.views
import clasterizator.views
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('contact_us/', views.contact, name='contact'),
    path('registerManager/', registerManager.views.controller,
         name="registerManager"),
    path('descriptionGenerator/', descriptionGenerator.views.home,
         name="descriptionGenerator"),
    path('titleGenerator/', titleGenerator.views.titleGenerator,
         name="titleGenerator"),
    path('clasterizator/', clasterizator.views.main,
         name="clasterizator"),
    path('stopwords/', stopwords.views.StopWords, name="StopWords"),
    path('adsgen/', adsgen.views.home_view, name="adsgen"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
