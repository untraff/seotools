import pymorphy2
from operator import itemgetter

COUNT_USAGE = 'количество_раз'
VARIANTS = 'варианты'
BEST_KWD = 'самое_популярное'

file = [
    'не выплачивают расчет при увольнении куда обратиться',
    'если не выплатили расчет при увольнении куда обратиться',
    'что делать если не выплачивают расчетные после увольнения',
    'если не выплачивают расчет при увольнении куда обратиться',
    'не рассчитывают при увольнении что делать',
]

data = {}
for row in file:
    tmp = row.split(' ')
    for keyword in tmp:
        normal_form = pymorphy2.MorphAnalyzer().parse(keyword)[0].normal_form
        # начинаем считать
        data.setdefault(normal_form,{})
        data[normal_form].setdefault(COUNT_USAGE,0)
        data[normal_form][COUNT_USAGE] += 1
        # записываем вариации
        data[normal_form].setdefault(VARIANTS,{})
        data[normal_form][VARIANTS].setdefault(keyword,0)
        data[normal_form][VARIANTS][keyword] += 1
# находим самую, частовстречающуюся фразу
for normal_form in data.keys():
    data[normal_form].setdefault(BEST_KWD, sorted(data[normal_form][VARIANTS].items(),key=itemgetter(1),reverse=True)[0][0])
# print(data)  
for k, v in sorted(data.items(), reverse=True, key = lambda tmp: tmp[1][COUNT_USAGE]):
    print(k, data[k])