from django.shortcuts import render, HttpResponse

def home(request):
    title = '!!!'
    description = 'Just drop your keywords here and each first letter of the word will become capitalize'
    return render(request, 'registerManager/registerManager.html', {'title':title, 'description':description, })

def controller(request):
    title = 'Make first letter capitalize'
    description = 'Just drop your keywords here and each first letter of the word will become capitalize'
    
    tmp = request.POST.get('keywords', 'Firstly, type some in upper form!!!1')
    debug = 'LOHNYA'
    keywords = tmp.split('\n')
    out = []
    for keyword in keywords:
        temporary_string = ''
        makeThisLetterBig = True
        for letter in keyword:
            if makeThisLetterBig:
                letter =  letter.upper()
                makeThisLetterBig = False
            temporary_string += letter
        out.append(temporary_string)

    return render(request, 'registerManager/registerManager.html', {'title':title, 'description':description,'keywords':out, 'basekeywords':tmp, 'debug':debug })
    
