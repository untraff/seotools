from django import forms


class ClasterizatorForm(forms.Form):
    keywords = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Keywords (each on newline \\n)'}))
    groups = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Groups (each on newline \\n)'}))
