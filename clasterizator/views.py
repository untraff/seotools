from django.shortcuts import render
import pymorphy2
from clasterizator.forms import ClasterizatorForm
from .models import KeyWord


def main(request):
    form = ClasterizatorForm()
    context = {
        'debug': 'debug',
        'form': form,
    }
    if request.method == 'POST':
        form = ClasterizatorForm(request.POST)
        if form.is_valid():
            keywords = form.cleaned_data['keywords'].split('\r\n')
            groups = form.cleaned_data['groups'].split('\r\n')

            new_keywords = []
            for keyword in keywords:
                new_keywords.append(KeyWord(keyword))

            categorys = []
            for group in groups:
                categorys.append(KeyWord(group))

            for keyword in new_keywords:
                for category in categorys:
                    keyword.check_category(category)
            context = {
                'new_keywords': new_keywords,
                'form': form,
            }
    return render(request, 'clasterizator/main.html', context)
