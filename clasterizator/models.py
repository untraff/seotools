from django.db import models
import pymorphy2


class Clasterizator():
    morph = pymorphy2.MorphAnalyzer()


class KeyWord(Clasterizator):
    def __init__(self, keyword):
        self.keyword = keyword
        self.make_normal_form()
        self.categorys = []

    def make_normal_form(self):
        self.normal = ''
        words = self.keyword.split(' ')
        for word in words:
            self.normal += ' ' + Clasterizator.morph.parse(word)[0].normal_form
        self.normal = self.normal.strip()
        return self.normal

    def check_category(self, category):
        found = False
        for category_normal_word in category.normal.split(' '):
            if not found:
                if category_normal_word in self.normal.split(' '):
                    self.categorys.append(category.keyword)
                    found = True
            else:
                return
