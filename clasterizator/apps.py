from django.apps import AppConfig


class ClasterizatorConfig(AppConfig):
    name = 'clasterizator'
