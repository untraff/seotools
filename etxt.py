import csv
from datetime import datetime, timedelta
from typing import Optional

from dicttoxml import dicttoxml
from pydantic import BaseModel

description = """
——————Общие требования
Тексты пишутся в информационно-аналитическом стиле. Это значит, что упор делается на факты, а не на описание. Задача текста: рассказать об особенностях товара и показать преимущество покупки у нас; Проанализируйте информацию у конкурентов, обобщите её, а затем вливайте в материал. Оперируйте фактами и не лейте воду. МОЖНО ДЕЛАТЬ НЕ ПРЯМОЕ ВХОЖДЕНИЕ КЛЮЧЕВОЙ ФРАЗЫ

——————Где проверять
https://content-watch.ru/text/ - уникальность 92,5%
https://glvrd.ru/ - 8.5 - главред даёт хорошие советы. Ориентируйтесь на него.

——————Использование ключевых фраз
Постарайтесь логически разбить текст на блоки по 600+- символов. В каждом таком блоке должна быть ключевая фраза

——————Структура
возьмите любые 5 абзацев из предложенных, для структы:
1 Назначение арматуры
2 Виды арматуры
3 ГОСТы арматуры
4 Производство арматуры
5 Классы арматуры
6 Классификация арматуры
7 Что нужно знать перед покупкой арматуры?
8 Как выбрать поставщика?
9 Где приобрести арматуру?

——————Лучше меньше, да лучше
Не растягивайте статью общими фразами. Они буду вырезаны из текста. Вместо этого подумайте, что еще, касательно этой темы, пригодится человеку и добавьте больше фактов. Рассказывайте об актуальном, интересном, злободневном. Текст должен не просто быть, а быть ради того, чтобы приносить пользу читателю. Ключевые запросы — это важное дополнение, которое всегда можно органично вписать. Но главное — содержание.
——————Важно
Наши статьи - коммерчески материалы которые преследуют 3 цели:
- Убедить человека что мы надежная компания, которая знает всё о изготовлении и продаже арматуры и др. металлопроката
- Убедить человека не откладывать а сделать заказ, позвонить, заполнить заявку на сайте, или воспользоваться чатом
- Убедить человек что покупка арматуры у нас это выгодно и надежно
"""


class TaskItem(BaseModel):
    title: str
    id_type: int
    id_subtype: int
    description: str
    deadline: str  # dd.mm.yyyy  .strftime("%d.%m.%Y")
    price: int
    price_type: int
    public: int
    category: str

    timeline: Optional[str] = "23:59"  # hh:mm

    keywords: Optional[str] = None
    text: Optional[str] = None
    whitespaces: Optional[int] = None
    size: Optional[int] = None
    uniq: Optional[int] = None
    auto_work: Optional[int] = None
    auto_rate: Optional[int] = None
    auto_level: Optional[int] = None
    auto_reports: Optional[int] = None
    auto_reports_n: Optional[int] = None
    only_stars: Optional[int] = None
    multicount: Optional[int] = None
    multione: Optional[int] = None
    user: Optional[str] = None
    whitelist: Optional[int] = None
    group_name: Optional[str] = None
    checksize: Optional[int] = None
    id_edu: Optional[int] = None
    id_sphere: Optional[int] = None
    attestat: Optional[str] = None


# читаем из файла
data_dict = {}
with open("data.csv", "r", encoding="UTF8") as fp:
    reader = csv.DictReader(fp)
    for row in reader:
        data_dict.setdefault(row["therme"], set())
        data_dict[row["therme"]].add(row["phrase"])

items = []
for k, v in data_dict.items():
    title = k
    keywords = ", ".join(v)
    today = datetime.today() + timedelta(5)
    item = TaskItem(
        title=title,
        id_type=4,
        id_subtype=2,
        description=description,
        deadline=today.strftime("%d.%m.%Y"),
        price=80,
        price_type=1,
        public=1,
        category="Стройматериалы",
        text=description,
        size=3000,
        keywords=keywords
    )
    items.append(item.dict(exclude_none=True, by_alias=True))

xml = dicttoxml(items, custom_root='items', attr_type=False)

with open("data.xml", "w", encoding="UTF8") as xml_fp:
    xml_fp.write(xml.decode(encoding="UTF8"))

