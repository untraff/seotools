import pymorphy2

arr = [
    'авиабилеты магас москва',
    'авиабилеты краснодар москва',
    'авиабилеты москва махачкала',
    'авиабилеты',
    'мальчишки',
    'сотовые телефоны москвы',
    'москва челябинск',
    'авиабилеты новосибирск москва',
    'авиабилеты владикавказ москва',
    'авиабилеты симферополь москва',
    'краснодар москва самолет',
    'авиабилеты москва магас',
    'магас москва авиабилеты',
    'москва севастополь',
    'москва сухум самолет',
    'авиабилеты омск москва',
    'волгоград тбилиси самолет',
    'магас москва',
    'краснодар москва',
    'авиабилеты москва душанбе внуково',
    'москва магас авиабилеты',
]
groups = [
    'москва',
    'краснодар',
    'севастополь',
]


class Clasterizator():
    morph = pymorphy2.MorphAnalyzer()


class KeyWord(Clasterizator):
    def __init__(self, keyword):
        self.keyword = keyword
        self.make_normal_form()
        self.categorys = []

    def make_normal_form(self):
        self.normal = ''
        words = self.keyword.split(' ')
        for word in words:
            self.normal += ' ' + Clasterizator.morph.parse(word)[0].normal_form
        self.normal = self.normal.strip()
        return self.normal

    def check_category(self, category):
        if category.normal in self.normal.split(' '):
            self.categorys.append(category.keyword)


keywords = []
for keyword in arr:
    keywords.append(KeyWord(keyword))

categorys = []
for group in groups:
    categorys.append(KeyWord(group))

for keyword in keywords:
    for category in categorys:
        keyword.check_category(category)
    print(keyword.__dict__)
