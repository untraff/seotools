from django.apps import AppConfig


class TitlegeneratorConfig(AppConfig):
    name = 'titleGenerator'
