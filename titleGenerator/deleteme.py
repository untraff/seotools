from operator import itemgetter
arr = {'статистика': {'статистика': 1}, 'по': {'по': 1}, 'слово': {'словам': 1}, 'пластиковый': {'пластиковые': 17, 'пластиковых': 6, 'пластиковое': 2}, 
        'окно': {'окна': 17, 'окон': 6, 'окно': 2}, 'цена': {'цены': 1, 'цена': 1}, 'купить': {'купить': 1}, 'установка': {'установка': 1, 'установкой': 1}, 'москва': {'москва': 1}}
# arr = sorted(arr.items(),  reverse=True)
bestForms = {}
# {'пластиковый': {'пластиковые': 17, 'пластиковых': 6, 'пластиковое': 2}, 'окно': {'окна': 17, 'окон': 6, 'окно': 2},}
for lemmatizedKeyword, toople in arr.items():
    sort = sorted(toople.items(), key=itemgetter(1),reverse=True)
    bestForms.setdefault(lemmatizedKeyword)
    bestForms[lemmatizedKeyword] = sort[0][0]
