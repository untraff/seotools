from django.shortcuts import render
import pymorphy2
from operator import itemgetter, attrgetter


def titleGenerator(request):
    morph = pymorphy2.MorphAnalyzer()
    title = 'Title and LSI generator'
    description = 'Analyse most popular keyphrases from ur list, and put in in future title, also get LSI queries'
    
    getkeyphrases = request.POST.get('keyphrases', 'пластиковые окна цена\r\nпластиковые окна в спб недорого\r\nпластиковые питер\r\nпластиковые окна купить')
    keyphrases = getkeyphrases.split('\r\n')
    options = [25,30,50,80,100,120,150,200,250]
    maxSymbols = int(request.POST.get('maxSymbols', '50'))

    normalKeywords = {}
    fullDict = {}
    for keyphrase in keyphrases:
        words = keyphrase.split(' ')
        for word in words:
            word = word.lower()
            normal_form = morph.parse(word)[0].normal_form
            # фомируем dict всех данных {normKeyword:{'keywords':string,'count':int}}
            fullDict.setdefault(normal_form, {})
            fullDict[normal_form].setdefault(word,0)
            fullDict[normal_form][word] += 1
            # формируем dict нормальных форм
            normalKeywords.setdefault(normal_form,0)
            normalKeywords[normal_form] += 1
    # сортируем полученный массив
    tmp = sorted(normalKeywords.items(), key=itemgetter(1), reverse=True)
    normalKeywords = tmp
    
    # {'пластиковый': {'пластиковые': 17, 'пластиковых': 6, 'пластиковое': 2}, 'окно': {'окна': 17, 'окон': 6, 'окно': 2},}
    bestForms = {}
    for lemmatizedKeyword, toople in fullDict.items():
        sort = sorted(toople.items(), key=itemgetter(1),reverse=True)
        bestForms.setdefault(lemmatizedKeyword)
        bestForms[lemmatizedKeyword] = sort[0][0]

    # лемматизированные ключи для LSI
    lemmatizedKeywords = []
    for k, v in tmp:
        lemmatizedKeywords.append((bestForms[k], v))

    # формирование title
    generatedTitle = ''
    for normalKeyword in normalKeywords:
        if len(generatedTitle) + 1 + len(bestForms[normalKeyword[0]]) < maxSymbols:
            generatedTitle += bestForms[normalKeyword[0]] + ' '

    debug = fullDict
    return render(request, 'titleGenerator/titleGenerator.html', 
                        {'debug':debug,'title':title,'description':description,'getkeyphrases':getkeyphrases,
                        'options':options, 'maxSymbols':maxSymbols,'keyphrases':keyphrases,'normalKeywords':lemmatizedKeywords,
                        'generatedTitle':generatedTitle,
                        })
