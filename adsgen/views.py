from django.shortcuts import render
from django.utils.text import slugify
from .forms import ContactForm


def home_view(request):
    form = ContactForm()
    result = []
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            keywords = form.cleaned_data['keywords'].split('\r\n')
            adgroups = form.cleaned_data['adgroups'].split('\r\n')
            urls = form.cleaned_data['urls'].split('\r\n')
            trusts = form.cleaned_data['trusts'].split('|')
            search_engine = form.cleaned_data['search_engine']
            adcampaign = form.cleaned_data['adcampaign']
            delimeter = form.cleaned_data['trusts_delimeter']
            if search_engine == 'yandex':
                header_1_len = 35
                header_2_len = 35
                text_1_len = 80
                text_2_len = 80
                utm_template = '?&utm_source=yandex&utm_medium=cpc&utm_campaign=cid|{campaign_id}|{source_type}&utm_content=gid|{gbid}|aid|{ad_id}|{phrase_id}_{retargeting_id}&utm_term={keyword}&utm_add_souce={source}&utm_region={region_name}|{region_id}&utm_position={position_type}|{position}&utm_device={device_type}'
            if search_engine == 'google':
                header_1_len = 30
                header_2_len = 30
                text_1_len = 80
                text_2_len = 80
                utm_template = '?utm_medium=cpc&utm_source=google_adwords&utm_campaign=nazvanie_kampanii|{campaignid}&utm_term={keyword}&utm_content=id|{targetid}|cid|{campaignid}|aid|{creative}|gid|{adgroupid}|pos|{adposition}|src|{network}_{placement}|dvc|{device}|reg|{loc_physical_ms}|rin|{loc_interest_ms}'
            header_1_default = form.cleaned_data['header_1_default']
            header_2_default = form.cleaned_data['header_2_default']
            text_1_before = form.data['text_1_before']
            text_1_after = form.data['text_1_after']
            fast_links_texts = form.cleaned_data['fast_links_texts'].split('|')
            text_2_default = form.cleaned_data['text_2_default']
            header_1_2_max_len = header_1_len + header_2_len
            # fast_links_urls = form.cleaned_data['fast_links_urls'].split('|')
            one_final_url = True
            if len(urls) == len(keywords):
                one_final_url = False
            keyword_header_split = form.cleaned_data['keyword_header_split']
            text_1_default = ''

            result = []
            result.append({'adcampaign': 'adcampaign', 'keyword': 'keyword', 'adgroup': 'adgroup', 'url': 'url', 'header_1': 'header_1', 'header_2': 'header_2',
                           'text_1': 'text_1', 'text_2': 'text_2', 'fast_links_urls': 'fast_links_urls', 'fast_links_texts': 'fast_links_texts'})
            for i in range(0, len(keywords)):
                row = {}
                keyword = keywords[i]
                # url
                adgroup = adgroups[i]
                if one_final_url:
                    url = urls[0]
                else:
                    url = urls[i]
                url += utm_template
                # headers:
                header_1 = ''
                header_2 = ''
                if not keyword_header_split:
                    if len(header_1 + keywords[i]) < header_1_len:
                        header_1 = keywords[i]
                    else:
                        header_1 = header_1_default
                    # header_2:
                    header_2 = header_2_default
                # разбить ключ по заголовкам
                else:
                    if len(keyword) < header_1_len:
                        header_1 = keyword
                        header_2 = header_2_default
                    elif len(keyword) > header_1_2_max_len:
                        header_1 = header_1_default
                        header_2 = header_2_default
                    else:
                        # основное
                        to_header_2 = False
                        separator = ''
                        for word in keyword.split(' '):
                            if to_header_2:
                                # ебашим во второй заголовок
                                header_2 += separator + word
                            else:
                                # ебашим в первый заголовое
                                if len(header_1 + separator + word) <= header_1_len:
                                    header_1 += separator + word
                                    separator = ' '
                                else:
                                    to_header_2 = True
                                    header_2 += word
                # text_1:
                text_1 = ''
                if len(text_1 + keyword) < text_1_len:
                    text_1 = keyword
                else:
                    text_1 = text_1_default
                if len(text_1 + text_1_before + text_1_after) < text_1_len:
                    text_1 = text_1_before + text_1 + text_1_after
                first_trust = True
                for i in range(0, len(trusts)):
                    if first_trust:
                        if len(text_1 + trusts[i]) < text_1_len:
                            text_1 += ' ' + trusts[i]
                            first_trust = False
                            continue
                    else:
                        if len(text_1 + trusts[i] + delimeter) < text_1_len:
                            text_1 += delimeter + trusts[i]
                # text_2:
                text_2 = text_2_default
                # fast_links:
                fast_links_urls = ''
                for i in range(0, len(fast_links_texts)):
                    if i == 0:
                        fast_links_urls += url + '#' + str(i)
                    else:
                        fast_links_urls += '|' + \
                            url + '#' + str(i)
                ready_fast_link_texts = ''
                for i in range(0, len(fast_links_texts)):
                    if i == 0:
                        ready_fast_link_texts += fast_links_texts[i]
                    else:
                        ready_fast_link_texts += '|' + fast_links_texts[i]
                # final:
                row.setdefault('adcampaign', adcampaign)
                row.setdefault('keyword', keyword)
                row.setdefault('adgroup', adgroup)
                row.setdefault('url', url)
                row.setdefault('header_1', header_1)
                row.setdefault('header_2', header_2)
                row.setdefault('text_1', text_1)
                row.setdefault('text_2', text_2)
                row.setdefault('fast_links_urls', fast_links_urls)
                row.setdefault('fast_links_texts', ready_fast_link_texts)
                result.append(row)

            for onerow in result:
                print(onerow['header_1'], '~~~',
                      onerow['header_2'], '~~~', onerow['text_1'])
    context = {
        'debug': result,
        'form': form,
    }
    return render(request, 'adsgen/main.html', context)
