from django import forms
import re


class ContactForm(forms.Form):
    use_required_attribute = True
    keywords = forms.CharField(label='', widget=forms.Textarea(
        attrs={"class": "form-control", 'rows': 5, 'placeholder': 'Пластиковые окна\nПластиковые окна купить в СПб'}), required=True)
    adgroups = forms.CharField(label='', widget=forms.Textarea(
        attrs={"class": "form-control", 'rows': 5, 'placeholder': 'Пластиковые окна\nПластиковые окна купить в СПб'}), required=True)
    urls = forms.CharField(label='', widget=forms.Textarea(
        attrs={"class": "form-control", 'rows': 5, 'placeholder': 'https://mydomain.ru/ \n#ОДИН УРЛ ИЛИ СТОЛЬКО ЖЕ СКОЛЬКО И КЛЮЧЕЙ'}), required=True)
    trusts = forms.CharField(label='', widget=forms.TextInput(
        attrs={"class": "form-control", 'placeholder': 'преимущество1|преимущество1|...|ПреимуществоN'}), required=True)
    search_engine = forms.ChoiceField(choices=(('yandex', 'yandex'), ('google', 'google')), widget=forms.Select(
        attrs={"class": "form-control"}), required=True)
    adcampaign = forms.CharField(label='', widget=forms.TextInput(
        attrs={"class": "form-control", 'placeholder': 'ГлавнаяПоиск'}), required=True)
    trusts_delimeter = forms.ChoiceField(choices=((' ', 'пробел'), (', ', ', '), ('; ', '; ')), widget=forms.Select(
        attrs={"class": "form-control"}), required=True)
    header_1_default = forms.CharField(label='', widget=forms.TextInput(
        attrs={"class": "form-control", 'placeholder': 'Если ключ не влезет...'}), required=True)
    header_2_default = forms.CharField(label='', widget=forms.TextInput(
        attrs={"class": "form-control", 'placeholder': 'Делай универсальный!'}), required=True)
    keyword_header_split = forms.BooleanField(widget=forms.CheckboxInput(
        attrs={"class": "form-check-input", }), required=False)
    fast_links_texts = forms.CharField(label='', widget=forms.TextInput(
        attrs={"class": "form-control", 'placeholder': 'один|два|три'}), required=True)
    fast_links_urls = forms.CharField(label='', widget=forms.TextInput(
        attrs={"class": "form-control disabled", 'disabled': 'disabled', 'placeholder': 'Можно оставить пустым, припишется #i'}), required=False)
    text_1_before = forms.CharField(label='', widget=forms.TextInput(
        attrs={"class": "form-control", 'placeholder': 'Текст перед ключом в тексте 1'}), required=False)
    text_1_after = forms.CharField(label='', widget=forms.TextInput(
        attrs={"class": "form-control", 'placeholder': 'Текст после ключа в тексте 1'}), required=False)
    text_2_default = forms.CharField(label='', widget=forms.Textarea(
        attrs={"class": "form-control", 'rows': 5, 'placeholder': 'Какой-то общий текст о вашем продукте'}), required=True)
    # def clean_phone(self):
    #     data = self.cleaned_data['phone']
    #     digits = re.findall('\d', data)
    #     if len(digits) < 10 or len(digits) > 11:
    #         raise forms.ValidationError(
    #             "Телефон должен содержать от 9 до 11 цифр, например +79214007030, а вы ввели " + str(len(digits)))
    #     return data

    # def clean_keywords(self):
    #     data = self.cleaned_data['keywords']
    #     if len(data) < 10:
    #         raise forms.ValidationError(
    #             "Пожалуйста, расскажите о вашем вопросе немного подробнее")
    #     return data
