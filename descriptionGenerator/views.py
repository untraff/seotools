from django.shortcuts import render


def home(request):
    title = 'Generate your descrption'
    description = 'This tool will help you generate SEO title, SEO description or some other stuff'

    getKeywords = request.POST.get(
        'keywords', 'что делать если ты дурачок и вляпался в наприятности\r\nторговля людьми ответственность ук рф\r\nукрал курицу срок')
    keywords = getKeywords.split('\r\n')

    getTrusts = request.POST.get(
        'trusts', '17 лет опыта,100% бесплатная консультация,1300+ выигранных дел,связи в судах,всё схвачено')
    trusts = getTrusts.split(',')

    maxSymbols = int(request.POST.get('maxSymbols', '80'))
    separator = request.POST.get('separator', ', ')
    textBefore = request.POST.get(
        'textBefore', 'Бесплатная консультация юриста: ')
    textAfter = ' — '

    options = [30, 50, 70, 80, 150, 200, 250]

    result = []
    for keyword in keywords:
        tmp = ''
        keyword = keyword.strip()
        if len(keyword) + len(textBefore) < maxSymbols:
            tmp += textBefore + keyword
        else:
            tmp += keyword
        needTextAfter = True
        for trust in trusts:
            if (needTextAfter == True):
                after = textAfter
            if len(tmp) + len(trust) + len(after) < maxSymbols:
                tmp += after + trust.strip()
                after = separator
                needTextAfter = False
        result.append(tmp)

    return render(request, 'descriptionGenerator/descriptionGenerator.html', {
        'title': title,
        'description': description,
        'result': result,
        'getKeywords': getKeywords,
        'getTrusts': getTrusts,
        'textBefore': textBefore,
        'separator': separator,
        'options': options,
        'maxSymbols': maxSymbols,
    }
    )
